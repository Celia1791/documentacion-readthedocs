html_context = {
    "display_gitlab": True, # Integrate Gitlab
    "gitlab_user": "MyUserName", # Username
    "gitlab_repo": "MyDoc", # Repo name
    "gitlab_version": "master", # Version
    "conf_py_path": "/source/", # Path in the checkout to the docs root
}



{% if display_gitlab %}
    <a href="https://{{ gitlab_host|default("gitlab.com") }}/
    {{ gitlab_user }}/{{ gitlab_repo }}/blob/{{ gitlab_version }}
    {{ conf_py_path }}{{ pagename }}{{ suffix }}" class="fa fa-gitlab">
    Edit on GitLab</a>
{% endif %}